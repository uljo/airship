package airship;

import java.util.Random;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class AirshipNode {

	public AirshipNode() {
		// TODO Auto-generated constructor stub
	}
	
	private Image img;
	private double x;
	private double y;
	
	private Random rand;
	private int dir; 			// -1 = sinking, 0 = steady, 1 = rising
	private int dirCount;		// num of times in update-loop
	double heightDiff = 0;
	
	private DropShadow dropShadow;
	
	public AirshipNode(String url, double size){
		img = new Image(url, size, 0, true, false);
		
		x = -1;
		y = -1;
		
		dir = 0;
		
		rand = new Random();
		
		dropShadow = new DropShadow();
        dropShadow.setBlurType(BlurType.GAUSSIAN);
        dropShadow.setColor(Color.BLACK);
        dropShadow.setOffsetX(15.0);
        dropShadow.setOffsetY(15.0);
        dropShadow.setRadius(20.0);
	}
	
	public void draw(GraphicsContext gc){
		
		if(img != null){
			
			double canvasHeight = gc.getCanvas().getHeight();
			
			if(x < 0){
				x = gc.getCanvas().getWidth()/2 - img.getWidth()/2;
				y = canvasHeight/2 - img.getHeight()/2;
			}
			
			gc.applyEffect(dropShadow);
			
			gc.drawImage(img, x, y);
			
			y += updatePosition();
			
			if(isAtBottom(canvasHeight)){
				y = canvasHeight - img.getHeight();
				dirCount = 0;
			}
			else if(isAtTop()){
				y = 0;
				dirCount = 0;
			}
		}
	}
	
	private double updatePosition(){
		
		if(dirCount >= 5){
			dir = rand.nextInt(3) - 1;
			dirCount = 0;
		
			
			if(dir == 0){
				heightDiff = 0;
			}
			else{
				double change = dir * rand.nextDouble()/5;
			
				heightDiff += change;
			}
		}
		
		dirCount++;
		
		//System.out.println(">> " + asDirText(dir) + " (" + dirCount + ") change: " + heightDiff);
		
		return heightDiff;
	}
	
	private String asDirText(int dir) {
		String text = "";
		switch(dir){
			case -1:
				text = "sinking";
				break;
			case 0:
				text = "steady";
				break;
			case 1:
				text = "rising";
				break;
		}
		return text;
	}

	private boolean isAtTop(){
		return y < 0;
	}
	
	private boolean isAtBottom(double canvasHeight){
		return y > (canvasHeight - img.getHeight());
	}

}
