package airship;

import java.util.Random;

import javafx.animation.AnimationTimer;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;

public class CountryPanel extends Group{

	private double speed = 0.9;
	private BackgroundNode bg;
	
	private double shipSize = 60;
	private AirshipNode ship;
	
	private Canvas canvas;

	public CountryPanel(double width, double height) {
		canvas = new Canvas(width, height);
	    getChildren().add(canvas);
	    
	    bg = new BackgroundNode("img/country/background-1.png", "Airship FX-102");
	    ship = new AirshipNode("img/transport/airship-1.png", shipSize);
	    
	    //final long startNanoTime = System.nanoTime();
	    
	    new AnimationTimer(){
	        public void handle(long currentNanoTime){
	            //double t = (currentNanoTime - startNanoTime) / 1000000000.0;
	            update();
	        }
	    }.start();
	}
	
	private void update(){
		
		GraphicsContext gc = canvas.getGraphicsContext2D();
		
		gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
		
		bg.draw(speed, gc);
		
		ship.draw(gc);
	}
	
	class BackgroundNode{
		
		private TextNode titleNode;
		
		private Image img;
		private double x;
		private double y;
		
		public BackgroundNode(String url, String title){
			img = new Image(url);
			
			x = 0;
			y = 0;
			
			titleNode = new TextNode(title);
		}
		
		public void draw(double speed, GraphicsContext gc){
			
			if(img != null){
				
				gc.drawImage(img, x, y);
				
				x -= speed;
				if(Math.abs(x) >= img.getWidth()/2){
					x = 0;
				}
			}
			
			if(titleNode != null)
				titleNode.draw(gc);
		}
	}
	
	class TextNode{
		
		private String text;
		
		public TextNode(String text){
			this.text = text;
		}
		
		public void draw(GraphicsContext gc){
			
			if(text != null){
				gc.setFill(Color.RED);
			    gc.setStroke(Color.BLACK);
			    gc.setLineWidth(2);
			    
			    Font theFont = Font.font("Times New Roman", FontWeight.BOLD, 48);
			    gc.setFont(theFont);
	
			    double x = canvas.getWidth()/2;
			    double y = 50;
			    
			    gc.setTextAlign(TextAlignment.CENTER);
		        gc.setTextBaseline(VPos.CENTER);
		        gc.fillText(text, x, y);
		        gc.strokeText(text, x, y);
			}
		}
	}

}
