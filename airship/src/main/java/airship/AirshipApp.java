package airship;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AirshipApp extends Application{
	
	private static final String APP_TITLE = "Airship";
	private int width = 800;
	private int height = 1000;

	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		
		stage.setTitle(APP_TITLE);
		 
	    boolean country = false;
	    Group root = getRoot(country);
	    
	    Scene scene = new Scene(root, width, height);
	    stage.setScene(scene);
	    
	    stage.show();
	}
	
	private Group getRoot(boolean country){
		return country ? new CountryPanel(width, height) : new CityPanel(width, height);
	}
}
