package airship;

import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class CityPanel extends Group{
	
	private Canvas canvas;
	
	private int bgLayerCount = 8;
	private BackgroundNode[] bgLayers = new BackgroundNode[bgLayerCount];
	
	private AirshipNode ship;
	
	public CityPanel(double width, double height) {
		canvas = new Canvas(width, height);
	    getChildren().add(canvas);
	    
	    for(int i = 0; i < bgLayerCount; i++){
	    	bgLayers[i] = new BackgroundNode("img/city/1920_1080/layer_0" + (i+1) + ".png");
	    }
	    
	    ship = new AirshipNode("img/transport/airship-1.png", 60);
	    
	    new AnimationTimer(){
	        public void handle(long currentNanoTime){
	            //double t = (currentNanoTime - startNanoTime) / 1000000000.0;
	            update();
	        }
	    }.start();
	}
	
	private void update(){
		
		GraphicsContext gc = canvas.getGraphicsContext2D();
		
		gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
		
		double speed = 0.1;
		for(int i = bgLayerCount - 1; i >= 0; i--){
			bgLayers[i].draw(speed, gc);
			speed += 0.2;
		}
		
		ship.draw(gc);
	}
	
	class BackgroundNode{
		
		private Image img;
		private double x;
		private double y;
		
		public BackgroundNode(String url){
			img = new Image(url);
			
			x = 0;
			y = 0;
		}
		
		public void draw(double speed, GraphicsContext gc){
			
			if(img != null){
				
				gc.drawImage(img, x, y);
				
				x -= speed;
				if(Math.abs(x) >= (img.getWidth() - canvas.getWidth())){
					x = 0;
				}
			}
		}
	}

}
